-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 31, 2018 at 01:07 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acuvera-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(51),
(51),
(51),
(51),
(51);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_token`
--

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_details`
--

CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_client_details`
--

INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) VALUES
('admin-client', 'oauth2-resource', '{bcrypt}$2a$10$zvzkjNmRe1lWT1COogimZuCKIV9PR57WSJcmoDMOHY1XLl/fa/pTu', 'read_all, write_all, update_all, delete_all', 'password,refresh_token,implicit', NULL, 'USER', 10800, 2592000, NULL, NULL),
('agent-client', 'oauth2-resource', '{bcrypt}$2a$10$zvzkjNmRe1lWT1COogimZuCKIV9PR57WSJcmoDMOHY1XLl/fa/pTu', 'read, write, update', 'password,refresh_token,implicit', NULL, 'USER', 10800, 2592000, NULL, NULL),
('customer-client', 'oauth2-resource', '{bcrypt}$2a$10$zvzkjNmRe1lWT1COogimZuCKIV9PR57WSJcmoDMOHY1XLl/fa/pTu', 'read_one, write_one, update_one, delete_one', 'password,refresh_token,implicit', NULL, 'USER', 10800, 2592000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_token`
--

CREATE TABLE `oauth_client_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_code`
--

CREATE TABLE `oauth_code` (
  `code` varchar(255) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_token`
--

CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `package_id` bigint(20) DEFAULT NULL,
  `pairing_date` datetime DEFAULT NULL,
  `pairing_location` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `agent_id`, `created_at`, `package_id`, `pairing_date`, `pairing_location`, `status`, `updated_at`, `user_id`) VALUES
(8, 2, '2018-08-26 00:06:50', 234, '2018-08-26 02:22:24', 'LONDON', 'PAIRED', '2018-08-26 00:06:50', 1),
(9, 2, '2018-08-26 00:06:50', 234, '2018-08-26 02:22:24', 'LONDON', 'PAIRED', '2018-08-26 00:06:50', 1),
(17, 2, '2018-08-26 00:08:22', 234, '2018-08-02 03:00:00', 'LONDON', 'altered', '2018-08-26 00:08:22', 1),
(18, 2, '2018-08-26 00:09:07', 234, '2018-08-02 03:00:00', 'LONDON', 'altered', '2018-08-26 00:09:07', 1),
(25, 2, '2018-08-26 11:02:32', NULL, '2018-08-02 03:00:00', 'LONDON', 'altered', '2018-08-26 11:02:32', 1),
(28, 2, '2018-08-26 11:03:58', NULL, '2018-08-02 03:00:00', 'LONDON', 'altered', '2018-08-26 11:03:58', 1),
(29, 2, '2018-08-26 11:05:38', NULL, '2018-08-02 03:00:00', 'LONDON', 'altered', '2018-08-26 11:05:38', 1),
(30, 2, '2018-08-26 11:06:41', NULL, '2018-08-02 03:00:00', 'NAIROBI', 'altered', '2018-08-26 11:06:41', 2),
(31, 2, '2018-08-26 11:08:57', 234, '2018-08-02 03:00:00', 'NAIROBI', 'altered', '2018-08-26 11:08:57', 2),
(32, 2, '2018-08-26 11:11:20', 234, '2018-08-02 03:00:00', 'NAIROBI', 'altered', '2018-08-26 11:11:20', 2),
(33, 2, '2018-08-26 11:13:49', 234, '2018-08-02 03:00:00', 'NAIROBI', 'altered', '2018-08-26 11:13:49', 2),
(34, 2, '2018-08-26 11:21:38', 234, '2018-08-02 03:00:00', 'NAIROBI', 'altered', '2018-08-26 11:21:38', 2),
(35, 2, '2018-08-26 11:26:04', 234, '2018-08-02 03:00:00', 'NAIROBI', 'ADDED', '2018-08-26 11:26:04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `package_items`
--

CREATE TABLE `package_items` (
  `package_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_items`
--

INSERT INTO `package_items` (`package_id`, `item_id`) VALUES
(34, 11),
(35, 23);

-- --------------------------------------------------------

--
-- Table structure for table `request_packaging`
--

CREATE TABLE `request_packaging` (
  `id` bigint(20) NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `assigned_date` datetime DEFAULT NULL,
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_packaging`
--

INSERT INTO `request_packaging` (`id`, `agent_id`, `assigned_date`, `request_date`, `status`, `updated_at`, `user_id`) VALUES
(15, 2, NULL, '2018-08-26 00:07:33', 'string', '2018-08-26 00:07:33', 1),
(16, 2, NULL, '2018-08-26 00:07:33', 'string', '2018-08-26 00:07:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `description`, `name`) VALUES
(0, 'super user', NULL),
(1, 'super user of the system', 'ADMIN'),
(2, 'packaging of bags', 'AGENT'),
(3, 'acuvera customer', 'CUSTOMER');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_token` varchar(255) DEFAULT NULL,
  `email_token_expiry` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_no_otp` int(11) DEFAULT NULL,
  `phone_no_otp_expiry` datetime DEFAULT NULL,
  `passport_country` varchar(255) DEFAULT NULL,
  `passport_no` varchar(255) DEFAULT NULL,
  `passport_upload` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tos` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `address`, `city`, `country`, `created_at`, `dob`, `email`, `email_token`, `email_token_expiry`, `enabled`, `first_name`, `last_name`, `phone_no_otp`, `phone_no_otp_expiry`, `passport_country`, `passport_no`, `passport_upload`, `password`, `phone_no`, `tos`, `updated_at`) VALUES
(1, 'nairobi', 'nairobi', 'kenya', '2018-08-28 23:45:29', '2018-08-23 03:00:00', 'c@b.c', NULL, NULL, b'1', 'MBARU', 'MURAYA', 735317, NULL, 'KENYA', 'ABC1234567890000000000', '/pass.png', '{bcrypt}$2a$10$zvzkjNmRe1lWT1COogimZuCKIV9PR57WSJcmoDMOHY1XLl/fa/pTu', '123456789', b'1', '2018-08-30 00:03:11'),
(2, 'nairobi', 'nairobi', 'kenya', '2018-08-28 23:45:29', '2018-08-23 03:00:00', 'a@b.c', NULL, NULL, b'1', 'MBARU', 'MURAYA', NULL, NULL, 'KENYA', 'ABC1234567890000000000', '/pass.png', '{bcrypt}$2a$10$Da7DdVQp1UrTkJHEvBkAK.d/4QLh5sC/ZGaSGLLxMxsjJPgkBnOM2', '1234567890', b'1', '2018-08-30 00:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_package_items`
--

CREATE TABLE `user_package_items` (
  `id` bigint(20) NOT NULL,
  `app_location` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) DEFAULT NULL,
  `item_pic_upload` varchar(255) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `receipt_upload` varchar(255) DEFAULT NULL,
  `refund_amount` double DEFAULT NULL,
  `refund_expiry` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tag_upload` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_package_items`
--

INSERT INTO `user_package_items` (`id`, `app_location`, `created_at`, `description`, `item_pic_upload`, `item_price`, `receipt_upload`, `refund_amount`, `refund_expiry`, `status`, `tag_upload`, `updated_at`, `user_id`) VALUES
(11, 'LONDON', '2018-08-26 00:07:59', 'LONDON', '/pic.png', 0, '/pic.png', 0, '2018-08-21 03:00:00', 'ADDED', '/pic/png', '2018-08-26 00:07:59', 1),
(23, 'LONDON', '2018-08-26 10:37:42', 'KENYA', '/pic.png', 0, '/pic.png', 0, '2018-08-21 03:00:00', 'VERIFIED', '/pic/png', '2018-08-26 11:26:04', 1),
(24, 'LONDON', '2018-08-26 10:37:42', 'KENYA', '/pic.png', 0, '/pic.png', 0, '2018-08-21 03:00:00', 'ADDED', '/pic/png', '2018-08-26 10:37:42', 1),
(26, 'LONDON', '2018-08-26 11:03:23', 'KENYA', '/pic.png', 0, '/pic.png', 0, '2018-08-21 03:00:00', 'ADDED', '/pic/png', '2018-08-26 11:03:23', 1),
(27, 'LONDON', '2018-08-26 11:03:23', 'KENYA', '/pic.png', 0, '/pic.png', 0, '2018-08-21 03:00:00', 'ADDED', '/pic/png', '2018-08-26 11:03:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(19, 3),
(20, 1),
(21, 3),
(22, 2),
(36, 1),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 1),
(50, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oauth_client_details`
--
ALTER TABLE `oauth_client_details`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcj3syi9lcukrlsh6o4llk0t8f` (`user_id`);

--
-- Indexes for table `package_items`
--
ALTER TABLE `package_items`
  ADD KEY `FKrjgvsd94q818wqs0hylinj0an` (`item_id`),
  ADD KEY `FKlk7ccw8ywkjidktb4627y7vbj` (`package_id`);

--
-- Indexes for table `request_packaging`
--
ALTER TABLE `request_packaging`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKgb90ua16kdiha209cmwx03m4p` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_package_items`
--
ALTER TABLE `user_package_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKmg2uoq2ljgnuurj4qbtyjkckc` (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `FKt7e7djp752sqn6w22i6ocqy6q` (`role_id`),
  ADD KEY `FKj345gk1bovqvfame88rcx7yyx` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
