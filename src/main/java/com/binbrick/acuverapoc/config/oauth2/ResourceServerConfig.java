package com.binbrick.acuverapoc.config.oauth2;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//    SCOPES
        private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/**").permitAll()
                //                    ADMIN ENDPOINTS
                .antMatchers(HttpMethod.GET, "/users").access("#oauth2.hasScope('admin_read')")
                .and()
                .csrf().disable();
    }

}
