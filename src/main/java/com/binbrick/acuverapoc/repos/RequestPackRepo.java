/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.repos;

import com.binbrick.acuverapoc.entities.PackagingRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emuraya
 */
@Repository
public interface RequestPackRepo  extends CrudRepository<PackagingRequest, Long>{
        PackagingRequest  findByid(Long id);
        Page<PackagingRequest> findAll(Pageable pg);

}
