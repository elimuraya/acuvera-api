/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.repos;

import com.binbrick.acuverapoc.entities.UserPackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emuraya
 */
@Repository
public interface PackagesRepo extends CrudRepository<UserPackage, Long> {
    UserPackage findByid(Long id);
    
    @Query("SELECT u FROM UserPackage u WHERE u.userId = ?1 and u.agentId = ?2 and u.status= ?3")
    Page<UserPackage> filterPackages(Long userId, Long agentId, String ststus, Pageable pg);
    Page<UserPackage> findAll(Pageable pg);
    
    
    
    

    
}
