/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.repos;

import com.binbrick.acuverapoc.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emuraya
 */
@Repository
public interface UserRepo extends CrudRepository<User, Long> {

    User findBypassportNo(String pass);

    User findByid(Long id);

    User findByphoneNo(String phone);

    User findByotp(String otp);

    User findByemailToken(String token);

    public User findByemail(String email);
    Page<User> findAll(Pageable pg);

}
