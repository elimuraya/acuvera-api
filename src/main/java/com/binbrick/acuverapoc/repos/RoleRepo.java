/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.repos;

import com.binbrick.acuverapoc.entities.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emuraya
 */
@Repository
public interface RoleRepo  extends CrudRepository<Role, Long>  {
    Role findByname(String name);
}
