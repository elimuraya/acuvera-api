/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.utils;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emuraya
 */
public class AcuveraChargingRate {

    public String chargingRate(ArrayList<Integer> range, int group, double high, double low) {
        String res = this.isRangeOk(range);
        if (res.matches("true") != true) {
            return res;

        } else {
            return "";
        }
    }

    private String isRangeOk(List<Integer> range) {
        if (range.size() > 2) {
            return "Provide only two parameters";
        } else if (range.isEmpty() == true) {
            return "Provide a range e.g [100,1000]";
        } else if (range.get(0) > range.get(1) || range.get(0) == range.get(1)) {
            return "Please provide a larger start index";
        } else if (range.get(1) - range.get(0) < 20) {
            return "provide bigger range";
        } else if (range.get(1) % range.get(0) != 0) {
            return "provide whole numbers";
        }
        return "true";
    }
//    private String checkParameters(int group, double high, double low) {
//        if ()
//        
//    }

}
