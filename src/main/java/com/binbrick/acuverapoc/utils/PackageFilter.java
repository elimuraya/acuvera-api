/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.utils;

/**
 *
 * @author emuraya
 */
public class PackageFilter {
    private Long userId;
    private Long agentId;
    private String status;

    public PackageFilter(Long userId, Long agentId, String status) {
        this.userId = null;
        this.agentId = null;
        this.status = null;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
