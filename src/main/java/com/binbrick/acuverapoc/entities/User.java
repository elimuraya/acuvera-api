/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author emuraya
 */
@Entity
@Table(name = "USERS")
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class User implements Serializable, UserDetails {
//    public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "DOB")
    private Date dob;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "CITY")
    private String city;
    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "EMAIL")
    private String email;
    @Column(name = "EMAIL_TOKEN")
    private String emailToken;
     @Column(name = "EMAIL_TOKEN_EXPIRY")
    private Date emailTokenExpiry;
    @Column(name = "PHONE_NO")
    private String phoneNo;
    @Column(name = "PHONE_NO_OTP")
    private String otp;
     @Column(name = "PHONE_NO_OTP_EXPIRY")
    private Date otpExpiry;
    @Column(name = "PASSPORT_NO")
    private String passportNo;
    @Column(name = "PASSPORT_COUNTRY")
    private String passportCountry;
    @Column(name = "PASSPORT_UPLOAD")
    private String passportUpload;
    @Column(name = "TOS")
    private boolean tos;

    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "ENABLED")
    private boolean enabled;

    @Transient
    private String userType;

    @Column(name = "CREATED_AT", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "UPDATED_AT", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @ManyToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE}, fetch =  FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
     //,  fetchType=FetchType.EAGER

    private Set<Role> roles;

    @OneToMany(mappedBy = "packages")
    private Collection<UserPackage> packages;

    @OneToMany(mappedBy = "items")
    private Collection<UserPackageItems> items;

    @OneToMany(mappedBy = "packageRequests")
    private Collection<PackagingRequest> packageRequests;

    public User(String firstName, String lastName, Date dob, String address, String city, String country, Integer pin, String email, 
            String emailToken, String phoneNo, String otp, String passportNo, String passportCountry, String passportUpload, boolean tos) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.address = address;
        this.city = city;
        this.country = country;
        this.password = password;
        this.email = email;
        this.emailToken = emailToken;
        this.phoneNo = phoneNo;
        this.otp = otp;
        this.passportNo = passportNo;
        this.passportCountry = passportCountry;
        this.passportUpload = passportUpload;
        this.tos = tos;
    }

    public User() {
    }

    public Date getEmailTokenExpiry() {
        return emailTokenExpiry;
    }

    public void setEmailTokenExpiry(Date emailTokenExpiry) {
        this.emailTokenExpiry = emailTokenExpiry;
    }

    public Date getOtpExpiry() {
        return otpExpiry;
    }

    public void setOtpExpiry(Date otpExpiry) {
        this.otpExpiry = otpExpiry;
    }

    
    
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportCountry() {
        return passportCountry;
    }

    public void setPassportCountry(String passportCountry) {
        this.passportCountry = passportCountry;
    }

    public String getPassportUpload() {
        return passportUpload;
    }

    public void setPassportUpload(String passportUpload) {
        this.passportUpload = passportUpload;
    }

    public boolean isTos() {
        return tos;
    }

    public void setTos(boolean tos) {
        this.tos = tos;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Collection<UserPackage> getPackages() {
        return packages;
    }

    public void setPackages(Collection<UserPackage> packages) {
        this.packages = packages;
    }

    public Collection<UserPackageItems> getItems() {
        return items;
    }

    public void setItems(Collection<UserPackageItems> items) {
        this.items = items;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Collection<PackagingRequest> getPackageRequests() {
        return packageRequests;
    }

    public void setPackageRequests(Collection<PackagingRequest> packageRequests) {
        this.packageRequests = packageRequests;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set authorities = new HashSet<Collection>();
        getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }

    @Override
    public String getUsername() {
        return phoneNo;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enabled;
    }
    
    
}
