/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;
import javax.persistence.Id;
import org.springframework.data.annotation.LastModifiedDate;

/**
 *
 * @author emuraya
 */
@Entity
@Table(name = "USER_PACKAGE_ITEMS")
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class UserPackageItems implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "RECEIPT_UPLOAD")
    private String receiptUpload;
    @Column(name = "TAG_UPLOAD")
    private String tagUpload;
    @Column(name = "ITEM_PRICE")
    private double itemPrice;
    @Column(name = "ITEM_PIC_UPLOAD")
    private String itemPicUpload;
    @Column(name = "REFUND_AMOUNT")
    private double refundAmount;
    @Column(name = "APP_LOCATION")
    private String appLocation;   
    @Column(name = "REFUND_EXPIRY")
    private Date refundExpiry;
    @Column(name = "STATUS")
    private String status;

    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "UPDATED_AT", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="USER_ID" , insertable=false, updatable=false)
    private User items;
    
    public UserPackageItems(){}

    public UserPackageItems(Long userId, String description, String receiptUpload, String tagUpload, double itemPrice, String itemPicUpload, double refundAmount, String appLocation, Date refundExpiry, String status) {
        this.userId = userId;
        this.description = description;
        this.receiptUpload = receiptUpload;
        this.tagUpload = tagUpload;
        this.itemPrice = itemPrice;
        this.itemPicUpload = itemPicUpload;
        this.refundAmount = refundAmount;
        this.appLocation = appLocation;
        this.refundExpiry = refundExpiry;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReceiptUpload() {
        return receiptUpload;
    }

    public void setReceiptUpload(String receiptUpload) {
        this.receiptUpload = receiptUpload;
    }

    public String getTagUpload() {
        return tagUpload;
    }

    public void setTagUpload(String tagUpload) {
        this.tagUpload = tagUpload;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemPicUpload() {
        return itemPicUpload;
    }

    public void setItemPicUpload(String itemPicUpload) {
        this.itemPicUpload = itemPicUpload;
    }

    public double getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(double refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getAppLocation() {
        return appLocation;
    }

    public void setAppLocation(String appLocation) {
        this.appLocation = appLocation;
    }

    public Date getRefundExpiry() {
        return refundExpiry;
    }

    public void setRefundExpiry(Date refundExpiry) {
        this.refundExpiry = refundExpiry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getItems() {
        return items;
    }

    public void setItems(User items) {
        this.items = items;
    }

    
    
    
}
