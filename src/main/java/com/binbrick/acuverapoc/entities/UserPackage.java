/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

/**
 *
 * @author emuraya
 */
@Entity
@Table(name = "PACKAGES")
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class UserPackage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "AGENT_ID")
    private Long agentId;
    @Column(name = "PACKAGE_ID")
    private Long deviceId;
    @Column(name = "PAIRING_LOCATION")
    private String pairingLocation;
    @Column(name = "PAIRING_DATE")
    private Date pairingDate;
    @Column(name = "STATUS")
    private String status;
    @Transient
    private List<Long> itemIds;

    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "UPDATED_AT", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "USER_ID", insertable = false, updatable = false)
    private User packages;

    @ManyToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE})
    @JoinTable(name = "PACKAGE_ITEMS", joinColumns = @JoinColumn(name = "PACKAGE_ID"), inverseJoinColumns = @JoinColumn(name = "ITEM_ID"))
    private Collection<UserPackageItems> packageItems;

    public UserPackage() {
    }

    public UserPackage(Long userId, Long packageId, String pairingLocation, Date pairingDate, String status, Long agentId) {
        this.userId = userId;
        this.agentId = agentId;
        this.deviceId = packageId;
        this.pairingLocation = pairingLocation;
        this.pairingDate = pairingDate;
        this.status = status;
    }

    public List<Long> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Long> itemIds) {
        this.itemIds = itemIds;
    }


    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getPairingLocation() {
        return pairingLocation;
    }

    public void setPairingLocation(String pairingLocation) {
        this.pairingLocation = pairingLocation;
    }

    public Date getPairingDate() {
        return pairingDate;
    }

    public void setPairingDate(Date pairingDate) {
        this.pairingDate = pairingDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getPackages() {
        return packages;
    }

    public void setPackages(User packages) {
        this.packages = packages;
    }

    public Collection<UserPackageItems> getPackageItems() {
        return packageItems;
    }

    public void setPackageItems(Collection<UserPackageItems> packageItems) {
        this.packageItems = packageItems;
    }

}
