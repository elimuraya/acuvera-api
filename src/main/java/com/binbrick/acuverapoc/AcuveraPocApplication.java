package com.binbrick.acuverapoc;

import com.binbrick.acuverapoc.utils.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class AcuveraPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcuveraPocApplication.class, args);
	}

}
