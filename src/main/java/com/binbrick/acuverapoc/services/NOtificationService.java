/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.services;

import java.util.List;
import java.util.Random;

import com.africastalking.AfricasTalking;
import com.africastalking.SmsService;
import com.africastalking.sms.Recipient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.binbrick.acuverapoc.utils.AfricasTalkingGateway;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emuraya
 */
@Service
public class NOtificationService {

    @Value("${sms.apikey}")
    private String key;
    @Value("${sms.username}")
    private String username;

    public void sendSMS(String numbers, String message)
    {


        /* Initialize SDK */
        AfricasTalking.initialize(username, key);

        /* Get the SMS service */
        SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);

        /* Set the numbers you want to send to in international format */
        String[] recipients = new String[] {
               numbers
        };
        System.out.print("SENDING MESSAGE TO  NUMBER =============== " +recipients.toString() + numbers);


        /* That’s it, hit send and we’ll take care of the rest */
        try {
            List<Recipient> response = sms.send(message, recipients, true);
            System.out.print(" MESSAGE RESPONSE =============== " +response.toString());

            for (Recipient recipient : response) {
                System.out.print("RECEPIENT NUMBER =============== " +recipient.number);
                System.out.print(" : ");
                System.out.println("STATUS =============== " + recipient.status);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }


    public boolean oldsendSMS(String recipients, String message) {

        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        // And of course we want our recipients to know what we really do
        // Create a new instance of our awesome gateway class
        AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, key);
        /**
         * ***********************************************************************************
         * NOTE: If connecting to the sandbox: 1. Use "sandbox" as the username
         * 2. Use the apiKey generated from your sandbox application
         * https://account.africastalking.com/apps/sandbox/settings/key 3. Add
         * the "sandbox" flag to the constructor AfricasTalkingGateway gateway =
         * new AfricasTalkingGateway(username, apiKey, "sandbox");
        *************************************************************************************
         */
        // Thats it, hit send and we'll take care of the rest. Any errors will
        // be captured in the Exception class below
        try {
            JSONArray results = gateway.sendMessage(recipients, message);
            for (int i = 0; i < results.length(); ++i) {
                JSONObject result = results.getJSONObject(i);
                System.out.print(result.getString("status") + ","); // status is either "Success" or "error message"
                System.out.print(result.getString("statusCode") + ",");
                System.out.print(result.getString("number") + ",");
                System.out.print(result.getString("messageId") + ",");
                System.out.println(result.getString("cost"));
            }
        } catch (Exception e) {
            System.out.println("Encountered an error while sending " + e.getSuppressed().toString());

        }
        return true;
    }

    public Integer generateToken() {
        Random r = new Random();
        var token = r.nextInt((999999 - 100000) + 1) + 100000;
        return token;
    }
    

}
