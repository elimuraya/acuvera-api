/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.services;

import com.binbrick.acuverapoc.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import com.binbrick.acuverapoc.repos.UserRepo;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.util.Collection;
import static java.util.Collections.emptyList;
import java.util.HashSet;
import java.util.Set;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author emuraya
 */
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepo urepo;

    @Override
    public UserDetails loadUserByUsername(String needle) {

        User usr = urepo.findByphoneNo(needle);
        if ( !usr.equals(null) ) {
            return usr;

        }
        throw new UsernameNotFoundException("User with phone number ["+ needle +"] not found");
    }

}
