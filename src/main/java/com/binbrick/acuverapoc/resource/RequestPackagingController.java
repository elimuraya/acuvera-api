/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.resource;

import com.binbrick.acuverapoc.entities.PackagingRequest;
import com.binbrick.acuverapoc.repos.RequestPackRepo;
import com.binbrick.acuverapoc.repos.UserRepo;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.security.Principal;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author emuraya
 */
@RestController
@RequestMapping("/packages/request")
public class RequestPackagingController {
        @Autowired
    UserRepo urepo;
    
        @Autowired
        RequestPackRepo rrepo;
        
         @GetMapping
    public ResponseEntity<ResponseWrapper> getAll(Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setData(rrepo.findAll(pg));
        return ResponseEntity.ok().body(wrap);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseWrapper> getRequest(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        if (rrepo.findById(Id).isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Request doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setData(rrepo.findById(Id));
        return ResponseEntity.ok().body(wrap);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseWrapper> deleteRequest(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        Optional<PackagingRequest> user = rrepo.findById(Id);

        if (user.isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Request doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        rrepo.deleteById(Id);
        wrap.setMessage("Request deleted successfully");
        return ResponseEntity.ok().body(wrap);
    }

    @PostMapping
    public ResponseEntity<ResponseWrapper> makeRequest(@RequestBody PackagingRequest usr, Principal principal) {
        ResponseWrapper wrap = new ResponseWrapper();
        usr.setUserId(urepo.findByphoneNo(principal.getName()).getId());

        rrepo.save(usr);
        wrap.setMessage("Request submitted successfully");
        return ResponseEntity.ok().body(wrap);

    }

    @PutMapping
    public ResponseEntity<ResponseWrapper> updateRequest(@RequestBody PackagingRequest usr) {
        ResponseWrapper wrap = new ResponseWrapper();

        PackagingRequest user = rrepo.findByid(usr.getId());

        if (user == null) {
            wrap.setCode(404);
            wrap.setMessage("Request doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }

        user.setAgentId(usr.getAgentId());
        user.setAssignedDate(usr.getAssignedDate());
        user.setStatus(usr.getStatus());
        
        rrepo.save(user);

        wrap.setMessage("Request updated successfully");
        return ResponseEntity.ok().body(wrap);

    }
        

    
}
