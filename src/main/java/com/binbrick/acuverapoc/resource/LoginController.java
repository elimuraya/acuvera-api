/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.resource;

import com.binbrick.acuverapoc.entities.User;
import com.binbrick.acuverapoc.repos.UserRepo;
import com.binbrick.acuverapoc.services.NOtificationService;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author emuraya
 */
@RestController
@RequestMapping("/auth")
public class LoginController {
    
    @Autowired
    UserRepo urepo;
    @Autowired
    NOtificationService nservice;
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @GetMapping("/profile")
    public ResponseEntity<ResponseWrapper> profile(Principal princ) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setData(urepo.findByphoneNo(princ.getName()));        
        return ResponseEntity.ok().body(wrap);
    }
    
    @PostMapping("/check-otp")
    public ResponseEntity<ResponseWrapper> checkOtp(@RequestParam("otp") String otp, @RequestParam("emailOtp") String emailOtp) {
        ResponseWrapper wrap = new ResponseWrapper();
        if (otp.isEmpty() != true) {
            User u = urepo.findByotp(otp);
            if (u != null) {
                
                u.setOtp(null);
                u.setOtpExpiry(null);
                u.setEnabled(true);
                
                urepo.save(u);
                
                wrap.setMessage("OTP VERIED SUCCESSFULLY");
                return ResponseEntity.ok().body(wrap);
            } else {
                wrap.setCode(404);
                wrap.setMessage("INVALID OTP TOKEN");
                return ResponseEntity.ok().body(wrap);
            }
            
        } else if (emailOtp.isEmpty() != true) {
            User u = urepo.findByemailToken(emailOtp);
            if (u != null) {
                u.setEmailToken(null);
                u.setEmailTokenExpiry(null);
                urepo.save(u);
                
                wrap.setMessage("EMAIL VERIED SUCCESSFULLY");
                return ResponseEntity.ok().body(wrap);
            } else {
                wrap.setCode(404);
                wrap.setMessage("INVALID EMAIL TOKEN");
                return ResponseEntity.ok().body(wrap);
            }
        }
        
        return ResponseEntity.ok().body(wrap);
    }
    
    @PostMapping("/reset-password")
    public ResponseEntity<ResponseWrapper> resetPassword(@RequestParam("email") String email) {
        ResponseWrapper wrap = new ResponseWrapper();
        User u = urepo.findByemail(email);
        if (u != null) {
            u.setEnabled(false);
            u.setEmailToken(nservice.generateToken().toString());  //generate and send to user via email and store in db
            urepo.save(u);

//            SEND EMAIL TO USER
            wrap.setMessage("REST LINK SENT TO EMAIL :" + email);
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setCode(404);
        wrap.setMessage("USER WITH EMAIL : " + email + " NOT FOUND");
        return ResponseEntity.ok().body(wrap);
    }
    
    @PutMapping("/reset-password")
    public ResponseEntity<ResponseWrapper> _resetPassword(@RequestParam("emailToken") String email,
            @RequestParam("password") String password) {
        ResponseWrapper wrap = new ResponseWrapper();
        User u = urepo.findByemailToken(email);
        if (u != null) {
//                user exists, update
            u.setPassword(passwordEncoder.encode(password));
            u.setEmailToken(null);
            u.setEmailTokenExpiry(null);
            u.setEnabled(true);
            urepo.save(u);
//            SEND EMAIL TO USER- password is changed
            wrap.setMessage("New password set successfuly. You can log in now");
            return ResponseEntity.ok().body(wrap);
        } else {
//                user doesnt exist, dont update
            wrap.setMessage("invalid token");
            return ResponseEntity.ok().body(wrap);
        }
        
    }
}
