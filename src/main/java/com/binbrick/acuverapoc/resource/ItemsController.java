/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.resource;

import com.binbrick.acuverapoc.entities.UserPackageItems;
import com.binbrick.acuverapoc.repos.PackageItemsRepo;
import com.binbrick.acuverapoc.repos.UserRepo;
import com.binbrick.acuverapoc.services.FIleUploadService;
import com.binbrick.acuverapoc.services.FileStorageException;
import com.binbrick.acuverapoc.services.NOtificationService;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author emuraya
 */
@RestController
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    PackageItemsRepo irepo;
    @Autowired
    FIleUploadService sservice;
    @Autowired
    NOtificationService nservice;
    @Autowired
    UserRepo urepo;

    @GetMapping
    public ResponseEntity<ResponseWrapper> getAll(Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setData(irepo.findAll(pg));
        return ResponseEntity.ok().body(wrap);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseWrapper> getItem(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        if (irepo.findById(Id).isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Item doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setData(irepo.findById(Id));
        return ResponseEntity.ok().body(wrap);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseWrapper> deleteItem(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        Optional<UserPackageItems> user = irepo.findById(Id);

        if (user.isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Item doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        irepo.deleteById(Id);
        wrap.setMessage("Item deleted successfully");
        return ResponseEntity.ok().body(wrap);
    }

    @PostMapping
    public ResponseEntity<ResponseWrapper> addItem(@RequestBody List<UserPackageItems> usr, Principal principal) {
        ResponseWrapper wrap = new ResponseWrapper();
        for (UserPackageItems u : usr) {
            u.setUserId(urepo.findByphoneNo(principal.getName()).getId());
        }

        wrap.setData(irepo.saveAll(usr));
        wrap.setMessage("Item(s) created successfully");
        return ResponseEntity.ok().body(wrap);

    }

    @PutMapping
    public ResponseEntity<ResponseWrapper> updateItem(@RequestBody UserPackageItems usr) {
        ResponseWrapper wrap = new ResponseWrapper();

        UserPackageItems user = irepo.findByid(usr.getId());

        if (user == null) {
            wrap.setCode(404);
            wrap.setMessage("Package doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }

        user.setUserId(usr.getUserId());
        user.setDescription(usr.getDescription());
        user.setReceiptUpload(usr.getReceiptUpload());
        user.setTagUpload(usr.getTagUpload());
        user.setItemPrice(usr.getItemPrice());
        user.setItemPicUpload(usr.getItemPicUpload());
        user.setRefundAmount(usr.getRefundAmount());
        user.setAppLocation(usr.getAppLocation());
//        to calculate from a predined date
        user.setRefundExpiry(usr.getRefundExpiry());
        user.setStatus(usr.getStatus());

        irepo.save(user);

        wrap.setMessage("Item updated successfully");
        return ResponseEntity.ok().body(wrap);
    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseWrapper> uploadItemPictures(Principal principal,
            @RequestParam("itemId") String itemId,
            @RequestParam("itemPicture") MultipartFile itemPicture,
            @RequestParam("itemReceipt") MultipartFile itemReceipt,
            @RequestParam("itemTag") MultipartFile itemTag) {
        ResponseWrapper wrap = new ResponseWrapper();
        String pictoken = nservice.generateToken().toString() + "_ITEMPIC_" + principal.getName();
        String recptoken = nservice.generateToken().toString() + "_ITEMRECPT_" + principal.getName();
        String tagtoken = nservice.generateToken().toString() + "_ITEMTAG_" + principal.getName();

        try {
//            fetch user from auth object, update below fields
            String _itemPicture = sservice.storeReceiptInfo(itemPicture, pictoken);
            String _itemReceipt = sservice.storeReceiptInfo(itemReceipt, recptoken);
            String _itemTag = sservice.storeReceiptInfo(itemTag, tagtoken);

//            fetch item by id from auth. update user
            UserPackageItems item = irepo.findByid(Long.valueOf(itemId));
            item.setItemPicUpload(_itemPicture);
            item.setReceiptUpload(_itemReceipt);
            item.setTagUpload(_itemTag);
            irepo.save(item);

            wrap.setCode(200);
            wrap.setMessage("upload successfully");
            wrap.setData(_itemPicture + "," + _itemReceipt + "," + _itemTag);
            return ResponseEntity.ok().body(wrap);
        } catch (FileStorageException ex) {
            wrap.setCode(302);
            wrap.setMessage(ex.getMessage());
            return ResponseEntity.ok().body(wrap);
        }
    }

    @GetMapping("/pictures/{filename}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = sservice.loadItemFile(filename);
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).contentType(MediaType.IMAGE_PNG)
                .body(file);
    }
}
