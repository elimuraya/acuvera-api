/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.resource;

import com.binbrick.acuverapoc.entities.Role;
import com.binbrick.acuverapoc.entities.User;
import com.binbrick.acuverapoc.repos.RoleRepo;
import com.binbrick.acuverapoc.repos.UserRepo;
import com.binbrick.acuverapoc.services.EmailServiceImpl;
import com.binbrick.acuverapoc.services.FIleUploadService;
import com.binbrick.acuverapoc.services.FileStorageException;
import com.binbrick.acuverapoc.services.NOtificationService;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author emuraya
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    EmailServiceImpl email;
    @Autowired
    UserRepo urepo;
    @Autowired
    RoleRepo rrepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    FIleUploadService sservice;
    @Autowired
    NOtificationService nservice;

    @GetMapping
    public ResponseEntity<ResponseWrapper> getAll(Authentication authentication, Principal principal, Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setData(urepo.findAll(pg));
        System.out.println(authentication.getName());
        System.out.println(principal.getName());

        return ResponseEntity.ok().body(wrap);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseWrapper> getUser(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        if (urepo.findById(Id) == null) {
            wrap.setCode(404);
            wrap.setMessage("User doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setData(urepo.findById(Id));
        return ResponseEntity.ok().body(wrap);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseWrapper> deleteUser(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        Optional<User> user = urepo.findById(Id);

        if (user.isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("User doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        urepo.deleteById(Id);
        wrap.setMessage("User deleted successfully");
        return ResponseEntity.ok().body(wrap);
    }

    @PutMapping
    public ResponseEntity<ResponseWrapper> updateUser(@RequestBody User usr) {
        ResponseWrapper wrap = new ResponseWrapper();
        User user = urepo.findByid(usr.getId());
        if (user == null) {
            wrap.setCode(404);
            wrap.setMessage("User doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }

        user.setFirstName(usr.getFirstName());
        user.setLastName(usr.getLastName());
        user.setDob(usr.getDob());
        user.setAddress(usr.getAddress());
        user.setCity(usr.getCity());
        user.setCountry(usr.getCountry());
        user.setEmail(usr.getEmail());
        user.setEmailToken(usr.getEmailToken());
        user.setPhoneNo(usr.getPhoneNo());
        user.setOtp(usr.getOtp());
        user.setPassportNo(usr.getPassportNo());
        user.setPassportCountry(usr.getPassportCountry());
        user.setPassportUpload(usr.getPassportUpload());
        user.setTos(usr.isTos());

        urepo.save(user);
        wrap.setMessage("User updated successfully");
        return ResponseEntity.ok().body(wrap);
    }

    @PostMapping
    public ResponseEntity<ResponseWrapper> registerUser(@RequestBody User usr) {
        ResponseWrapper wrap = new ResponseWrapper();

        String role = usr.getUserType();
        usr.setPassword(passwordEncoder.encode(usr.getPassword()));
        usr.setOtp(nservice.generateToken().toString());
        usr.setEmailToken(nservice.generateToken().toString());

        if (urepo.findByphoneNo(usr.getPhoneNo()) != null) {
            wrap.setCode(400);
            wrap.setMessage("User with phone number " + usr.getPhoneNo() + " already exists");
            return ResponseEntity.ok().body(wrap);
        } else if (urepo.findByemail(usr.getEmail()) != null) {
            wrap.setCode(400);
            wrap.setMessage("User with email address " + usr.getEmail() + " already exists");
            return ResponseEntity.ok().body(wrap);
        }

        if (urepo.findBypassportNo(usr.getPassportNo()) == null) {
            if (role.matches("CUSTOMER") == true) {
                usr.setRoles(new HashSet<Role>(Arrays.asList(rrepo.findByname("CUSTOMER"))));
                urepo.save(usr);
                wrap.setMessage("User customer  created successfully");
                nservice.sendSMS(usr.getPhoneNo(), "OTP : " + usr.getOtp().toString());
                email.sendSimpleMessage(usr.getEmail(), "Account Registration", "Account registered successfully. Use the code below to confirm your email " + usr.getEmailToken() + "");

                return ResponseEntity.ok().body(wrap);
            } else if (role.matches("ADMIN") == true) {
                usr.setRoles(new HashSet<Role>(Arrays.asList(rrepo.findByname("ADMIN"))));
                urepo.save(usr);
                wrap.setMessage("User admin created successfully");
                nservice.sendSMS(usr.getPhoneNo(), "OTP : " + usr.getOtp().toString());
                email.sendSimpleMessage(usr.getEmail(), "Account Registration", "Account registered successfully. Use the code below to confirm your email " + usr.getEmailToken() + "");

                return ResponseEntity.ok().body(wrap);
            } else if (role.matches("AGENT") == true) {
                usr.setRoles(new HashSet<Role>(Arrays.asList(rrepo.findByname("AGENT"))));
                urepo.save(usr);
                wrap.setMessage("User agent created successfully");
                nservice.sendSMS(usr.getPhoneNo(), "OTP : " + usr.getOtp().toString());
                email.sendSimpleMessage(usr.getEmail(), "Account Registration", "Account registered successfully. Use the code below to confirm your email " + usr.getEmailToken() + "");

                return ResponseEntity.ok().body(wrap);
            }
            wrap.setCode(404);
            wrap.setMessage("User type " + role + " not found");
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setMessage("User with passport number " + usr.getPassportNo() + " exists");
        return ResponseEntity.ok().body(wrap);
    }

    @GetMapping("/type")
    public ResponseEntity<ResponseWrapper> getUserType(@RequestParam("type") String type, Pageable pg) {
        return this.getAllUserByType(type, pg);
    }

    public ResponseEntity<ResponseWrapper> getAllUserByType(String type, Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
        HashSet<User> users = new HashSet<User>(Arrays.asList());

        for (User u : urepo.findAll()) {
            if (u.getRoles().contains(rrepo.findByname(type)) == true) {
                users.add(u);
            }
        }
        if (users.isEmpty() == true) {
            wrap.setCode(404);
            wrap.setMessage("No users found of type " + type);
            return ResponseEntity.ok().body(wrap);
        } else {

            wrap.setData(users);
            return ResponseEntity.ok().body(wrap);
        }

    }

//    **************UTILITIES*******************
    @PostMapping("/upload/{id}")
    public ResponseEntity<ResponseWrapper> uploadPassport(@RequestParam("passportUpload") MultipartFile file, @PathVariable("id") Long id) {
        ResponseWrapper wrap = new ResponseWrapper();
        User u = urepo.findByid(id);
        if (u == null) {
            wrap.setCode(404);
            wrap.setMessage("User not found");
            return ResponseEntity.ok().body(wrap);
        } else {

            try {

               String location =  sservice.storePassport(file, nservice.generateToken().toString() + "_PASSPORT_" + u.getPhoneNo() + "_");
                u.setPassportUpload(location);
                urepo.save(u);
                wrap.setCode(200);
                wrap.setMessage("Upload Successfully :" + location);
                return ResponseEntity.ok().body(wrap);
            } catch (FileStorageException ex) {
                wrap.setCode(302);
                wrap.setMessage(ex.getMessage());
                return ResponseEntity.ok().body(wrap);
            }

        }
    }

	@GetMapping("/passport/{filename}")
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = sservice.loadPassportFile(filename);
		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).contentType(MediaType.IMAGE_PNG)
				.body(file);
	}
}
