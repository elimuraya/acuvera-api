/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binbrick.acuverapoc.resource;

import com.binbrick.acuverapoc.entities.Role;
import com.binbrick.acuverapoc.entities.UserPackage;
import com.binbrick.acuverapoc.entities.UserPackageItems;
import com.binbrick.acuverapoc.repos.PackageItemsRepo;
import com.binbrick.acuverapoc.repos.PackagesRepo;
import com.binbrick.acuverapoc.utils.PackageFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import com.binbrick.acuverapoc.utils.ResponseWrapper;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import org.springframework.data.domain.Pageable;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author emuraya
 */
@RestController
@RequestMapping("/packages")
public class PackageResource {
    
    @Autowired
    PackagesRepo prepo;
    @Autowired
    PackageItemsRepo irepo;
    
    @GetMapping
    public ResponseEntity<ResponseWrapper> getAll(Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setData(prepo.findAll(pg));
        return ResponseEntity.ok().body(wrap);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<ResponseWrapper> getPackage(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        if (prepo.findById(Id).isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Package doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        wrap.setData(prepo.findById(Id));
        return ResponseEntity.ok().body(wrap);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseWrapper> deletePackage(@PathVariable("id") Long Id) {
        ResponseWrapper wrap = new ResponseWrapper();
        Optional<UserPackage> user = prepo.findById(Id);
        
        if (user.isPresent() == false) {
            wrap.setCode(404);
            wrap.setMessage("Package doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        prepo.deleteById(Id);
        wrap.setMessage("Package deleted successfully");
        return ResponseEntity.ok().body(wrap);
    }
    
    @PostMapping
    public ResponseEntity<ResponseWrapper> makePackage(@RequestBody UserPackage usr) {
        ResponseWrapper wrap = new ResponseWrapper();
        HashSet<UserPackageItems> packageItems = new HashSet<UserPackageItems>(Arrays.asList());
        String notfound = " ";
//                        new HashSet<Role>(Arrays.asList(rrepo.findByname("ADMIN"))));

        for (Long id : usr.getItemIds()) {
            if (irepo.findById(id).isPresent() == true) {
                packageItems.add(irepo.findByid(id));
            } else {
                notfound = notfound + " " + id;
            }
        }
        
        if (notfound.matches(" ") == false) {
            wrap.setMessage("Items with id(s) " + notfound + " not found");
            return ResponseEntity.ok().body(wrap);
        } else {
            for (UserPackageItems item : packageItems ){
                item.setStatus("VERIFIED");
            }
            irepo.saveAll(packageItems);
            
            usr.setPackageItems(packageItems);            
            prepo.save(usr);
            wrap.setMessage("Package created successfully");
            return ResponseEntity.ok().body(wrap);
        }
        
    }
    
    @PutMapping
    public ResponseEntity<ResponseWrapper> updatePackage(@RequestBody UserPackage usr) {
        ResponseWrapper wrap = new ResponseWrapper();
        
        UserPackage user = prepo.findByid(usr.getId());
        
        if (user == null) {
            wrap.setCode(404);
            wrap.setMessage("Package doesnt exist");
            return ResponseEntity.ok().body(wrap);
        }
        
        user.setAgentId(usr.getAgentId());
        //generate random string
//        user.setPackageId();
        user.setPairingDate(usr.getPairingDate());
        user.setPairingLocation(usr.getPairingLocation());
        user.setStatus(usr.getStatus());
        user.setUserId(usr.getUserId());
        
        prepo.save(user);
        
        wrap.setMessage("Package updated successfully");
        return ResponseEntity.ok().body(wrap);
    }
//    DYNAMIC FILTER
     @GetMapping("/filter")
    public ResponseEntity<ResponseWrapper> filter(PackageFilter filter, Pageable pg) {
        ResponseWrapper wrap = new ResponseWrapper();
//        System.out.println("USER ID :"+filter.getUserId().toString() + "AGENT ID " +filter.getAgentId().toString() + "STATUS" +filter.getStatus().toString());
        
        wrap.setData(prepo.filterPackages( filter.getUserId(), filter.getAgentId(), filter.getStatus(), pg));
        return ResponseEntity.ok().body(wrap);
    }
}
